//alert("Hello World!");

//ACTIVITY 24

// Add a function in the Address blueprint that will print the whole address(use the backticks to create the resulting string.)

// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

const getCube = (num) => num**3;

let num = 2;
let numCube = getCube(num);


console.log(`The cube of ${num} = ${numCube}`);



// 5. Create a variable address with a value of an array containing details of an address.

let address = ["258 Washington Ave", "NW, California", 90011];

let [streetAddress, areaAddress, numberAddress] = address;
console.log(`I live at ${streetAddress} ${areaAddress} ${numberAddress}`);
// console.log(streetAddress);
// console.log(areaAddress);
// console.log(numberAddress);



// 7. Create a variable animal with a value of an object data type with different animal details as it's properties.

let animal = {
	animalName: "Lolong",
	animalType: "saltwater crocodile",
	animalWeight: 1075,
	animalLengthFoot: 20,
	animalLengthInch: 3
}

//Object Destructuring: use {} instead of []
let {animalName, animalType, animalWeight, animalLengthFoot, animalLengthInch} = animal;

function getAnimalDetails(animalObject) {
	console.log(`${animalName} was a ${animalType}. He weighed at ${animalWeight} kgs with a measurement of ${animalLengthFoot} ft ${animalLengthInch} in.`);
}

getAnimalDetails(animal);

// 9. Create an array of number. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

let numbers = [1,2,3,4,5];

//Loop using forEach
// numbers.forEach(function(numbers) {
// 	console.log(numbers);
// });

//Arrow Function using implicit return statement
numbers.forEach((numbers) => console.log(numbers));



// 11. Create a class Dog and constructor that will accept a name, age and breed as it's properties

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let asoDog = new Dog();
asoDog.name = "Danger";
asoDog.age = 69;
asoDog.breed = "Pitbull";

console.log(asoDog);